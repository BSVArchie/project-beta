from django.urls import path
from .api_views import api_list_techs, api_tech, api_list_appointments, api_appointment, api_cancel, api_finished

urlpatterns = [
    path("technicians/", api_list_techs, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_tech, name="api_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/<int:pk>/cancel/", api_cancel, name="api_cancel"),
    path("appointments/<int:pk>/finish/", api_finished, name="api_cancel"),
]
