from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class Appointment(models.Model):
    date = models.DateField()
    time = models.CharField(max_length=100)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=100, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="tech",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
