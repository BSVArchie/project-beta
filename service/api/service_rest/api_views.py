from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

from .models import Technician, Appointment, AutomobileVO



class TechsListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]

@require_http_methods(["GET", "POST"])
def api_list_techs(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"technicians": techs},
            encoder=TechsListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechsListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_tech(request, pk):
    if request.method == "GET":
        tech = Technician.objects.get(id=pk)
        return JsonResponse(
            {"tech": tech},
            encoder=TechsListEncoder,
            safe=False
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})









class AppointmentsEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date",
        "time",
        "reason",
        "status",
        "vin",
        "customer",
        "vip",
        "technician",
        ]

    # def get_extra_data(self, o):
    #     return {"tech_first": o.technician.first_name, "tech_last": o.technician.last_name, "employee_id": o.technician.employee_id}
    encoders = {
        "technician": TechsListEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentsEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        # print(content)
        try:
            tech_id = content["technician"]
            technician = Technician.objects.get(pk=tech_id)
            content["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400
            )

        sold_vins = AutomobileVO.objects.all()
        vin_list = []
        for vin in sold_vins:
            vin_list.append(vin.vin)
        # print(vin_list)
        if content["vin"] in vin_list:
            content["vip"] = True
        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentsEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentsEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def api_cancel(request, pk):
    # content = json.loads(request.body)
    Appointment.objects.filter(id=pk).update(status = "canceled")
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentsEncoder,
        safe=False
    )


@require_http_methods(["PUT"])
def api_finished(request, pk):
    # content = json.loads(request.body)
    Appointment.objects.filter(id=pk).update(status = "finished")
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentsEncoder,
        safe=False
    )
