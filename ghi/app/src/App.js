import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import Models from './Models';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';
import ModelForm from './ModelForm';
import Salespeople from './Salespeople';
import SalespersonForm from './SalespersonForm';
import Customer from './Customer';
import CustomerForm from './CustomerForm';
import Sales from './Sales';
import SalesForm from './SalesForm';
import TechniciansList from './TechniciansList';
import TechnicianForm from './TechnicianForm';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import AppointmentsHistory from './AppointmentsHistory';
import SalesHistory from './SalesHistory';

function App() {

  const [appointments, setAppointments] = useState([])

  async function getAppointments() {
      const response = await fetch('http://localhost:8080/api/appointments/')
      if (response.ok) {
          const data = await response.json()
          // console.log(data)
          setAppointments(data.appointments)
      }
  }

  useEffect(() => {
      getAppointments();
    }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/automobiles/create" element={<AutomobileForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/salespeople" element={<Salespeople />} />
          <Route path="/salespeople/create" element={<SalespersonForm />} />
          <Route path="/models" element={<Models />} />
          <Route path="/models/create" element={<ModelForm />} />
          <Route path="/customers" element={<Customer />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path="/sales/create" element={<SalesForm />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/technicians/create" element={<TechnicianForm />} />
          <Route path="/appointments" element={<AppointmentsList appointments={appointments} getAppointments={getAppointments} />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/appointments/history" element={<AppointmentsHistory appointments={appointments} getAppointments={getAppointments} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
