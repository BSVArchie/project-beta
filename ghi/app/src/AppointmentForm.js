import React, { useEffect, useState } from 'react';

function AppointmentForm() {

    const [technicians, setTechnicians] = useState([])

    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [technician, setTech] = useState('')
    const [reason, setReason] = useState('')

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleDateChange = (event) => {
        const value = event.target.value
        setDate(value)
    }

    const handleTimeChange = (event) => {
        const value = event.target.value
        setTime(value)
    }

    const handleTechChange = (event) => {
        const value = event.target.value
        setTech(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }


    const fetchTechs = async () => {
        const urlTechs = 'http://localhost:8080/api/technicians/'

        const response = await fetch(urlTechs)
        if (response.ok) {
            const data = await response.json()
            // console.log(data)
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        fetchTechs();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.vin = vin
        data.customer = customer
        data.date = date
        data.time = time
        data.technician = technician
        data.reason = reason
        // console.log(data)

        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            const appointmentCreated= await response.json()
            console.log(appointmentCreated)

            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setTech('')
            setReason('')
        }
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a service appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input onChange={handleVinChange} placeholder="Automobile VIN" required type="text" className="form-control" value={vin} />
                  <label htmlFor="name">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleCustomerChange} placeholder="Customer" required type="text" className="form-control" value={customer} />
                  <label htmlFor="name">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleDateChange} placeholder="Date" required type="date" className="form-control" value={date} />
                  <label htmlFor="name">Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleTimeChange} placeholder="Time" required type="time" className="form-control" value={time} />
                  <label htmlFor="name">Time</label>
                </div>
                <select required onChange={handleTechChange} className="form-select">
                <option value="">Technician</option>
                {technicians.map(tech => {
                  return (
                    <option key={tech.id} value={tech.id}>
                      {tech.employee_id}
                    </option>
                  )
                })}
              </select>
              <div className="form-floating mb-3">
                  <input onChange={handleReasonChange} placeholder="Reason" required type="text" className="form-control" value={reason} />
                  <label htmlFor="name">Reason</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      );
}

export default AppointmentForm
