
import React, { useState, useEffect } from 'react';

function SalesHistory() {
    const [sales, setSales] = useState([]);
    const [salespersons, setPersons] = useState([]);
    const [salesperson, setPerson] = useState("");

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
          const data = await response.json();
            console.log(data)
          setSales(data.sales)
          setPerson(data.sales.salesperson)
        }
        const salespersonurl = "http://localhost:8090/api/salespeople/"
        const salespersonresponse = await fetch(salespersonurl);

        if (salespersonresponse.ok) {
          const data = await salespersonresponse.json();
          setPersons(data.salespersons);
        }
      }


      useEffect(()=>{
        getData()
      }, [])

      function handleChangeSalesperson(event) {
        const { value } = event.target;
        setPersons(value);
    }

  return (
    <div className="sales-container">
        <h1>Salesperson History</h1>
          <div className="form-floating mb-3">
              <select
                  onChange={handleChangeSalesperson}
                  required
                  name="salespersons"
                  id="salespersons"
                  className="form-select">
                  <option value="">Choose Salesperson</option>
                  {salespersons.map((salesperson) => {
                      return (
                          <option key={salesperson.id} value={salesperson.id}>
                              {salesperson.first_name} {salesperson.last_name}
                          </option>
                      )
                  }
                  )};
                </select>
          </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sales) => {
            if (sales.automobile.sold === true) {
            return (
              <tr key={sales.id}>
                <td>{ sales.salesperson.first_name} { sales.salesperson.last_name}</td>
                <td>{ sales.customer.first_name} { sales.customer.last_name}</td>
                <td>{ sales.automobile.vin }</td>
                <td>{ sales.price }</td>

              </tr>
            );
            }
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistory;
