import { useState, useEffect } from 'react';

function AppointmentsHistory({ appointments, getAppointments }) {

    const [vin, setVin] = useState('')

    // let search = appointments

    // const handleSearch = (event) => {
    //     const value = event.target.value
    //     setVin(value)
    // }

    // const searchVin = () => {
    //     for (const obj of search) {
    //         if (obj.vin === vin) {
    //             search = []
    //             search.push(obj)
    //         }
    //     }
    //     getAppointments()
    //     console.log(search)
    // }

    // useEffect(() => {
    //     searchVin();
    //   }, []);

    return (
        <div>
            <h1>Service History</h1>
            <div>
                <label htmlFor="site-search"></label>
                <input onChange={event => setVin(event.target.value)} type="search"  placeholder="Search by VIN" name="q" />

                {/* <button onClick={() => searchVin(vin)}>Search</button> */}
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((val)=> {
                        if (vin == "") {
                            return val
                        } else if (val.vin.includes(vin)) {
                            return val
                        }
                    }).map((appointment) => {
                        return (
                            <tr key={appointment.vin}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip ? 'yes' : 'no'}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentsHistory
