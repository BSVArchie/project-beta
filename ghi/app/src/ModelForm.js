import React, { useState, useEffect } from 'react';

function ModelForm () {
    const [name, setModel] = useState('');
    const [picture_url, setPicture] = useState('');
    const [manufacturer_id, setManufacturer] = useState('');
    const [manufacturer_ids, setManufacturers] = useState([]);



    async function fetchData() {
        const url = "http://localhost:8100/api/manufacturers/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
            // console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name,
            picture_url,
            manufacturer_id,
        };
        console.log(data)
        const manUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(manUrl, fetchConfig);
        if (response.ok) {
            const newModel = await response.json()
            console.log(newModel)
            setModel("");
            setPicture(data.picture_url);
            setManufacturer("");
        }
    }

    function handleChangeModel(event) {
        const { value } = event.target;
        setModel(value);
    }

    function handleChangePicture(event) {
        const { value } = event.target;
        setPicture(value);
    }

    function handleChangeManufacturer(event) {
        const { value } = event.target;
        setManufacturer(value);
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Model</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                    <input
                        value={name}
                        onChange={handleChangeModel}
                        placeholder="models"
                        required
                        type="text"
                        name="models"
                        id="models"
                        className="form-control"
                        autoComplete="name"
                    />
                    <label htmlFor="models">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={picture_url}
                        onChange={handleChangePicture}
                        placeholder="Picture"
                        required
                        type="text"
                        name="picture_url"
                        id="picture_url"
                        className="form-control"
                    />
                    <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div className="form-floating mb-3">
                    <select
                        value={manufacturer_id}
                        onChange={handleChangeManufacturer}
                        placeholder="manufacturer"
                        required
                        name="manufacturer_id"
                        id="manufacturer_id"
                        className="form-select">
                        <option value="">Choose Manufacturer</option>
                        {manufacturer_ids.map((manufacturer) => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
        </div>
    );
}

export default ModelForm;
