import React, { useState } from 'react';

function SalespersonForm () {
    const [first_name, setFirst] = useState('');
    const [last_name, setLast] = useState('');
    const [employee_id, setEmployee] = useState('');


    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };
        console.log(data)
        // make sure to have post showing the right port
        const Url = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(Url, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json()
            console.log(newSalesperson)
            setFirst("");
            setLast("");
            setEmployee("");
        }
    }

    function handleChangeFirst(event) {
        const { value } = event.target;
        setFirst(value);
    }

    function handleChangeLast(event) {
        const { value } = event.target;
        setLast(value);
    }

    function handleChangeEmployee(event) {
        const { value } = event.target;
        setEmployee(value);
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <div className="form-floating mb-3">
                    <input
                        value={first_name}
                        onChange={handleChangeFirst}
                        placeholder="First_name"
                        required
                        type="text"
                        name="first_name"
                        id="first_name"
                        className="form-control"
                        autoComplete="first_name"
                    />
                    <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={last_name}
                        onChange={handleChangeLast}
                        placeholder="Last_name"
                        required
                        type="text"
                        name="last_name"
                        id="last_name"
                        className="form-control"
                    />
                    <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={employee_id}
                        onChange={handleChangeEmployee}
                        placeholder="Employee_ID"
                        required
                        type="text"
                        name="employee_id"
                        id="employee_id"
                        className="form-control"
                    />
                    <label htmlFor="employee_id">Employee ID</label>
                    </div>

                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
        </div>
    );
}

export default SalespersonForm;
