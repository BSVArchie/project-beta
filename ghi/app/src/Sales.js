import React, { useState, useEffect } from 'react';


function Sales() {
  const [sales, setSales] = useState([])


  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const data = await response.json();
        console.log(data)
      setSales(data.sales)
    
    }

  }


  useEffect(()=>{
    getData()
  }, [])


  return (
    <div className="sales-container">
        <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>

          </tr>
        </thead>
        <tbody>
          {sales.map(sales => {
          if (sales.automobile.sold === true) {
            return (
              <tr key={sales.id}>
                <td>{ sales.salesperson.employee_id }</td>
                <td>{ sales.salesperson.first_name} { sales.salesperson.last_name}</td>
                <td>{ sales.customer.first_name} { sales.customer.last_name}</td>
                <td>{ sales.automobile.vin }</td>
                <td>{ sales.price }</td>


              </tr>
          )};
 })}
        </tbody>
      </table>
    </div>

        );
}
export default Sales;
