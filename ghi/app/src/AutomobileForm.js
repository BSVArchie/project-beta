import React, { useEffect, useState } from 'react';

function AutomobileForm() {
    const [models, setModels] = useState([])

    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            // console.log(data)
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model
        // console.log(data)

        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(autoUrl, fetchConfig)
        if (response.ok) {
            const newAuto = await response.json()
            console.log(newAuto)

            setColor('')
            setYear('')
            setVin('')
            setModel('')
        }

    }


    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add an automobile to inventory</h1>
              <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Vehicle Color" required type="text" name="color" id="color" className="form-control" value={color} />
                  <label htmlFor="name">Vehicle Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleYearChange} placeholder="Year ex: 1996" required type="number" name="year" id="year" className="form-control" value={year} />
                  <label htmlFor="name">Year</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleVinChange} placeholder="17 digit VIN" required type="text" name="vin" id="vin" className="form-control" value={vin} />
                  <label htmlFor="name">VIN</label>
                </div>
                <select required onChange={handleModelChange} name="model" id="model" className="form-select">
                <option value="">Model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  )
                })}
              </select>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      );
}

export default AutomobileForm
