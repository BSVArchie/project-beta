import React, { useState } from 'react';

function CustomerForm () {
    const [first_name, setFirst] = useState('');
    const [last_name, setLast] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhone] = useState('');


    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            address,
            phone_number,
        };
        console.log(data)
        // make sure to have post showing the right port
        const Url = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(Url, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json()
            console.log(newCustomer)
            setFirst("");
            setLast("");
            setAddress("");
            setPhone("");
        }
    }

    function handleChangeFirst(event) {
        const { value } = event.target;
        setFirst(value);
    }

    function handleChangeLast(event) {
        const { value } = event.target;
        setLast(value);
    }

    function handleChangeAddress(event) {
        const { value } = event.target;
        setAddress(value);
    }

    function handleChangePhone(event) {
        const { value } = event.target;
        setPhone(value);
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">
                    <div className="form-floating mb-3">
                    <input
                        value={first_name}
                        onChange={handleChangeFirst}
                        placeholder="First_name"
                        required
                        type="text"
                        name="first_name"
                        id="first_name"
                        className="form-control"
                        autoComplete="first_name"
                    />
                    <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={last_name}
                        onChange={handleChangeLast}
                        placeholder="Last_name"
                        required
                        type="text"
                        name="last_name"
                        id="last_name"
                        className="form-control"
                    />
                    <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={address}
                        onChange={handleChangeAddress}
                        placeholder="Address"
                        required
                        type="text"
                        name="address"
                        id="address"
                        className="form-control"
                    />
                    <label htmlFor="address">Address</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={phone_number}
                        onChange={handleChangePhone}
                        placeholder="Phone_number"
                        required
                        type="text"
                        name="phone_number"
                        id="phone_number"
                        className="form-control"
                    />
                    <label htmlFor="phone_number">Phone Number</label>
                    </div>

                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
        </div>
    );
}

export default CustomerForm;
