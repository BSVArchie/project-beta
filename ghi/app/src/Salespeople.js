import React, { useState, useEffect } from 'react';


function Salespeople() {
  const [salespeople, setSalespeople] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const data = await response.json();
        console.log(data)
      setSalespeople(data.salespersons)
    }

  }


  useEffect(()=>{
    getData()
  }, [])


  return (
    <div className="salespeople-container">
      <h1>Salespeople</h1>
      <table className="table table-striped">

        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(salespeople => {

            return (
              <tr key={salespeople.id}>
                <td>{ salespeople.employee_id }</td>
                <td>{ salespeople.first_name }</td>
                <td>{ salespeople.last_name }</td>

              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Salespeople;
