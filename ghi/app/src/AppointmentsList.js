// import { useState, useEffect } from 'react';

function AppointmentsList({ appointments, getAppointments }) {

    // const [appointments, setAppointments] = useState([])

    // async function getAppointments() {
    //     const response = await fetch('http://localhost:8080/api/appointments/')
    //     if (response.ok) {
    //         const data = await response.json()
    //         console.log(data)
    //         setAppointments(data.appointments)
    //     }
    // }

    // useEffect(() => {
    //     getAppointments();
    //   }, []);

    const cancelAppointment = async(id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(cancelUrl, fetchConfig)
        const appointmentCanceled = await response.json()
        console.log(appointmentCanceled)
        getAppointments()
    }

    const finishAppointment = async(id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/finish/`
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(cancelUrl, fetchConfig)
        const appointmentFinished = await response.json()
        console.log(appointmentFinished)
        getAppointments()
    }

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map( appointment => {
                        if (appointment.status === "created") {
                            return (
                                <tr key={appointment.vin}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.vip ? 'yes' : 'no'}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button type="button" id={appointment.vin} onClick={() => cancelAppointment(appointment.id)} className="btn btn-danger">Cancel</button>
                                    </td>
                                    <td>
                                        <button type="button" id={appointment.vin} onClick={() => finishAppointment(appointment.id)} className="btn btn-success">Finished</button>
                                    </td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentsList
