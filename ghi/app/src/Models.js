import React, { useState, useEffect } from 'react';


function Models() {
  const [model, setModel] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
        console.log(data)
      setModel(data.models)
    }

  }


  useEffect(()=>{
    getData()
  }, [])


  return (
    <div className="model-container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {model.map(model => {

            return (
              <tr key={model.id}>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td>
                  <img
                    src={`${model.picture_url}`}
                    className="img-thumbnail"
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Models;
