import React, { useState, useEffect } from 'react';

function SalesForm () {
    const [automobiles, setVins] = useState([]);
    const [automobile, setVin] = useState("");
    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState("");
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");




    async function fetchVins() {
        const url = "http://localhost:8100/api/automobiles/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setVins(data.autos)
            // console.log(data)
        }
    }

    async function fetchSalespersons() {
        const url = "http://localhost:8090/api/salespeople/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons)
            console.log(data)
        }
    }

    async function fetchCustomers() {
        const url = "http://localhost:8090/api/customers/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
            // console.log(data)
        }
    }

    useEffect(() => {
        fetchVins();
        fetchSalespersons();
        fetchCustomers();
    }, [])



    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            automobile,
            salesperson,
            customer,
            price,
        };
        console.log(data)
        // make sure to have post showing the right port
        const Url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        // const UrlSold = `http://localhost:8100/api/automobiles/${automobile.vin}`;
        // const fetchSold = {
        //     method: "put",
        //     body: JSON.stringify({"sold": true}),
        //     headers: {
        //         "Content-Type": "application/json",
        //     },
        // };

        const response = await fetch(Url, fetchConfig);
        if (response.ok) {
            const newSales = await response.json()
            console.log(newSales)
            setVin("");
            setSalesperson("");
            setCustomer("");
            setPrice("");
        }

    //     const sold = await fetch(UrlSold, fetchSold);
    //     if (sold.ok) {
    //         const newSold = await response.json()
    //         console.log(newSold)
    // }
}

    function handleChangeVins(event) {
        const { value } = event.target;
        setVin(value);
    }

    function handleChangeSalesperson(event) {
        const { value } = event.target;
        setSalesperson(value);
    }

    function handleChangeCustomer(event) {
        const { value } = event.target;
        setCustomer(value);
    }

    function handleChangePrice(event) {
        const { value } = event.target;
        setPrice(value);
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Sale</h1>
                <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="form-floating mb-3">
                    <select
                        value={automobile}
                        onChange={handleChangeVins}
                        placeholder="Automobile"
                        required
                        type="text"
                        name="automobile"
                        id="automobile"
                        className="form-select">
                        <option value="">Choose Automobile Vin</option>
                        {automobiles.map((automobiles) => {
                            if (automobiles.sold === false) {
                            return (
                                <option key={automobiles.id} value={automobiles.vin}>
                                    {automobiles.vin}
                                </option>
                            );
                            }
                        })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                    <select
                        value={salesperson}
                        onChange={handleChangeSalesperson}
                        placeholder="Salespersons"
                        required
                        type="text"
                        name="salespersons"
                        id="salespersons"
                        className="form-control">
                        <option value="">Choose Salesperson</option>
                        {salespersons.map((salespersons) => {
                            return (
                                <option key={salespersons.id} value={salespersons.id}>
                                    {salespersons.employee_id}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                    <select
                        value={customer}
                        onChange={handleChangeCustomer}
                        placeholder="Customer"
                        required
                        type="text"
                        name="customer"
                        id="customer"
                        className="form-control">
                        <option value="">Choose Customer</option>
                        {customers.map((customers) => {
                            return (
                                <option key={customers.id} value={customers.id}>
                                    {customers.first_name} {customers.last_name}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        value={price}
                        onChange={handleChangePrice}
                        placeholder="Price"
                        required
                        type="text"
                        name="price"
                        id="price"
                        className="form-control"
                    />
                    <label htmlFor="Price">Price</label>
                    </div>

                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
        </div>
    );
}

export default SalesForm;
