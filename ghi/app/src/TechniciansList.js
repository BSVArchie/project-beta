import { useState, useEffect } from 'react';

function TechniciansList() {

    const [techs, setTechs] = useState([])

    async function getTechs() {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const data = await response.json();
            // console.log(data)
            setTechs(data.technicians)
        }
    }

    useEffect(() => {
        getTechs();
      }, []);

    return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {techs.map( tech => {
                        return (
                            <tr key={tech.employee_id}>
                                <td>{tech.employee_id}</td>
                                <td>{tech.first_name}</td>
                                <td>{tech.last_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechniciansList
