import React, { useEffect, useState } from 'react';

function TechnicianForm() {

    const [fName, setFname] = useState('')
    const [lName, setLname] = useState('')
    const [employeeID, setEmployeeID] = useState('')

    const handleFnameChange = (event) => {
        const value = event.target.value
        setFname(value)
    }

    const handleLnameChange = (event) => {
        const value = event.target.value
        setLname(value)
    }

    const handleEmployeeChange = (event) => {
        const value = event.target.value
        setEmployeeID(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.first_name = fName
        data.last_name = lName
        data.employee_id = employeeID
        // console.log(data)

        const url = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newTech = await response.json()
            console.log(newTech)

            setFname('')
            setLname('')
            setEmployeeID('')
        }
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Technician</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFnameChange} placeholder="First Name..." required type="text" className="form-control" value={fName} />
                  <label htmlFor="name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLnameChange} placeholder="Last Name..." required type="text" className="form-control" value={lName} />
                  <label htmlFor="name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmployeeChange} placeholder="Employee ID..." required type="text" className="form-control" value={employeeID} />
                  <label htmlFor="name">Employe ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      );
}

export default TechnicianForm
