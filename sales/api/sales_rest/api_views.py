from django.views.decorators.http import require_http_methods
from .models import Sale, Customer, AutomobileVO, Salesperson
import json

from django.http import JsonResponse
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):

    if request.method == "GET":

        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)

            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except json.JSONDecodeError:
            return JsonResponse(
                {"error": "client error nerd"},
                status=400,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request, id):
    try:
        if request.method == "GET":
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            count, _ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        else:
            content = json.loads(request.body)

            Salesperson.objects.filter(id=id).update(**content)
            salespersons = Salesperson.objects.get(id=id)
            return JsonResponse(
                salespersons,
                encoder=SalespersonEncoder,
                safe=False,
            )
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"error": "Salesperson not here"},
            status=404
        )
    except json.JSONDecodeError:
        return JsonResponse(
            {"error": "client error nerd"},
            status=400
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":

        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)

            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except json.JSONDecodeError:
            return JsonResponse(
                {"error": "client error nerd"},
                status=400,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, id):
    try:
        if request.method == "GET":
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        else:
            content = json.loads(request.body)

            Customer.objects.filter(id=id).update(**content)
            customers = Customer.objects.get(id=id)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"error": "Customer does not exist"},
            status=404
        )
    except json.JSONDecodeError:
        return JsonResponse(
            {"error": "client error nerd"},
            status=400
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":

        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"error": "Salesperson does not exist"},
                status=404
            )

        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"error": "Customer does not exist"},
                status=404
            )

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "Vin does not exist"},
                status=404
            )

        sale = Sale.objects.create(**content)
        print(content)
        return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )


@require_http_methods("GET")
def api_show_sales(request, id):
    try:
        if request.method == "GET":
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        else:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
                )
    except Sale.DoesNotExist:
        return JsonResponse(
            {"error": "Sale does not exist"},
        )
    except json.JSONDecodeError:
        return JsonResponse(
            {"error": "client error nerd"}
        )
