from django.urls import path
from .api_views import (
    api_list_salespersons,
    api_show_salesperson,
    api_list_customers,
    api_show_customer,
    api_list_sales,
    api_show_sales
)


urlpatterns = [
    path("salespeople/", api_list_salespersons, name="api_create_salesperson"),
    path("salespeople/<int:id>/", api_show_salesperson, name="api_delete_salesperson"),
    path("customers/", api_list_customers, name="api_create_customer"),
    path("customers/<int:id>/", api_show_customer, name="api_delete_customer"),
    path("sales/", api_list_sales, name="api_create_sale"),
    path("sales/<int:id>/", api_show_sales, name="api_detail_sale"),
]
