# CarCar

Team:

* Matt Archbold - Service microservice
* Tay - Sales microservice?

## Set up ##

    1. Fork repository
      https://gitlab.com/BSVArchie/project-beta

    2.git clone project

    3. Build and run project
        docker volume create beta-data
        docker-compose build
        docker-compose up

============================================================================================================

## Design
    See image CarCar.png in this folder

    The CarCar application has three bounded services(Inventory, Services, and Sales).

    The services have a Django back-end and use APIs to GET, POST, PUT, and DELETE data about automobiles, service techs/appointments, sales/salespeople, and customers.

    The CarCar application use a React front-end to allow users to easily interface with data from all services.


============================================================================================================

*************************************************************************************************************



## Service microservice

    Django:

        * Models:

        1. Technician
            first_name
            last_name
            employee_id

        2. AutomobileVO(pulled from inventory)
            vin(17 Character unique identifier)
            sold(boolean default=False)


        3. Appointment
            date
            time
            reason
            status (default=created)
            vin (CharField !!!Not from AutomobileVO)
            customer
            vip (boolean)
            technician(foreign key)

        ----------------------------------------------------------------------------------------------------------

            Action					           Method				        URL

            ### Technicians ###

        List technicians			            GET 	    http://localhost:8080/api/technicians/
            JSON body: N/A
        Create a technician			            POST	    http://localhost:8080/api/technicians/
            JSON body: {
                    "first_name": "test",
                    "last_name": "Delete",
                    "employee_id": "testtodelete"
                    }
        Delete a specific technician		    DELETE	    http://localhost:8080/api/technicians/:id/
            JSON body: N/A



             ### Service Appointments

        List appointments			            GET		    http://localhost:8080/api/appointments/
            JSON body: N/A
        Create an appointment			        POST	    http://localhost:8080/api/appointments/
            JSON body: {
                    "date": "2024-03-24",
                    "time": "9:00:00 AM",
                    "reason": "Oil change",
                    "vin": "195480385949328",
                    "customer": "Mark Hall",
                    "technician": "1"
                }
        Delete an appointment			        DELETE	    http://localhost:8080/api/appointments/:id/
            JSON body: N/A
        Set appointment status to "canceled"	PUT		    http://localhost:8080/api/appointments/:id/cancel/
            JSON body: {
                    "status": "canceled"
                }
        Set appointment status to "finished"	PUT		    http://localhost:8080/api/appointments/:id/finish/
             JSON body: {
                    "status": "finished"
                }

        Automobile Poller (service/poll/poller.py)          http://project-beta-inventory-api-1:8000/api/automobiles/
        AutomobileVO every 60 seconds with updated VINs & sold status

        -----------------------------------------------------------------------------------------------------------

        *Views

        Technicians:
        @require_http_methods(["GET", "POST"])
        def api_list_techs(request):

        @require_http_methods(["GET", "DELETE"])
        def api_tech(request, pk):


        Appointments:
        @require_http_methods(["GET", "POST"])
        def api_list_appointments(request):

        @require_http_methods(["GET", "DELETE"])
        def api_appointment(request, pk):
            POST checks vin against AutomobileVO vin and changes VIP if true

        @require_http_methods(["PUT"])
        def api_cancel(request, pk):
            sets appointment status to canceled

        @require_http_methods(["PUT"])
        def api_finished(request, pk):
            sets appointment status to finished

        ------------------------------------------------------------------------------------------------------

        # React


            # React

            List all technicians:
                (Nav.js) /technicians
                <Route path="/technicians" element={<TechniciansList />} />
                TechniciansList() get objct from service/service and renders a table of techs
            Add a technician
                (Nav.js) /technicians/create:
                <Route path="/technicians" element={<TechniciansList />} />
                TechnicianForm() Renders form for user to "POST" info for a new tech to the database


            List all service appointments:
                (Nav.js) /appointments
                <Route path="/appointments" element={<AppointmentsList appointments={appointments}
                    getAppointments={getAppointments} />} />
                getAppointments() #In apps.js props for AppointmentsList.
                    AppointmentsList renders a list of current(created appointments)

                Special Feature 2: Appointment Status
                    cancelAppointment() and finishAppointment() allow user to click a button to up an
                        appointments statue in the database

            Create a service appointment:
                (Nav.js) /appointments/create
                <Route path="/appointments/create" element={<AppointmentForm />} />
                AppointmentForm() renders a form for users to create an appointment

                Special Feature 1: VIP
                    Backend function sets VIP status when AppointmentForm is submitted


            Appointment history:
                (Nav.js) /appointments/history
                <Route path="/appointments/history" element={<AppointmentsHistory
                    appointments={appointments} getAppointments={getAppointments} />} />
                AppointmentsHistory renders a list of all appointments regardless of their status
                    and has VIN search bar function.

<!-- Explain your models and integration with the inventory
microservice, here. -->

*************************************************************************************************************

## Sales microservice

 Django:

        * Models:

        1. Salesperson
            first_name
            last_name
            employee_id

        2. AutomobileVO(pulled from inventory)
            vin(17 Character unique identifier)
            sold(boolean default=False)

        3. Customer
            first_name
            last_name
            address
            phone_number

        3. Sales
            automobile
            salesperson
            customer
            price

        ----------------------------------------------------------------------------------------------------------

            Action					           Method				        URL

            ### Salesperson ###

        List salesperson			            GET 	    http://localhost:8090/api/salespeople/
            JSON body: N/A
        Create a technician			            POST	    http://localhost:8090/api/salespeople/
            JSON body: {
                    "first_name": "bill",
                    "last_name": "josh",
                    "employee_id": "joshbill"
                    }
        Delete a specific salesperson		    DELETE	    http://localhost:8090/api/salespeople/:id/
            JSON body: N/A



             ### Sales

        List sales			            GET		    http://localhost:8090/api/sales/
            JSON body: N/A
        Create an appointment			        POST	    http://localhost:8090/api/sales/
            JSON body: {
                    "salesperson": "2",
	                "customer": "2",
	                "automobile": "1C3CC5FB2AN120198",
	                "price": "25000.99"
                }
        Delete an salepeople			        DELETE	    http://localhost:8090/api/salespeople/:id/

                    ### customer ###

        List salesperson			            GET 	    http://localhost:8090/api/customers/
            JSON body: N/A
        Create a technician			            POST	    http://localhost:8090/api/customers/
            JSON body: {
                    "first_name": "bobby",
                    "last_name": "bob",
                    "address": "bobbyton",
                    "phone_number": "1010101010"
                    }
        Delete a specific salesperson		    DELETE	    http://localhost:8090/api/customers/:id/
            JSON body: N/A


        Automobile Poller (service/poll/poller.py)          http://project-beta-inventory-api-1:8000/api/automobiles/
        AutomobileVO every 60 seconds with updated VINs & sold status

        -----------------------------------------------------------------------------------------------------------

        *Views

        salesperson:
        require_http_methods(["GET", "POST"])
        def api_list_salespersons(request):
            create salesperson

        require_http_methods(["DELETE", "GET", "PUT"])
        def api_show_salesperson(request, id):
            shows salesperson


        Customers:
        require_http_methods(["GET", "POST"])
        def api_list_customers(request):
            create customer

        require_http_methods(["DELETE", "GET", "PUT"])
        def api_show_customer(request, id):
            shows customers

        @require_http_methods(["GET", "POST"])
        def api_list_sales(request):
            create sale

        @require_http_methods("GET")
        def api_show_sales(request, id):
            shows sales

        ------------------------------------------------------------------------------------------------------

        # React


            # React

            List all salesperson:
                (Nav.js) /salespeople
                <Route path="/salespeople" element={<Salespeople />} />
                Salespeople()  renders a table of salespeople
            Add a salesperson
                (Nav.js) /salespeople/create:
                <Route path="/salespeople/create" element={<SalespersonForm />} />
                SalespersonForm() Renders form for user to "POST" info for a new salespeople to the database


            List all sales:
                (Nav.js) /sales
                <Route path="/sales" element={<Sales />} />
                Sales()  renders a table of salespeople
                    Sale Renders form for user to "POST" info for a new sale to the database


            Create a sale:
                (Nav.js) /sales/create
                <Route path="/sales/create" element={<SalesForm />} />
                SalesForm() renders a form for users to create an sales



            sale history:
                (Nav.js) /sale/history
                <Route path="/sales/history" element={<SalesHistory />} />
                SalesHistory() renders a list of all sales that have happened
                isnt able to drop down but does show a list of sold 

              List all customers:
                (Nav.js) /customers
                <Route path="/customers" element={<Customer />} />
                Salespeople() renders a table of customers
            Add a customer
                (Nav.js) /salespeople/create:
                <Route path="/customers/create" element={<CustomerForm />} />
                customerForm() Renders form for user to "POST" info for a new customer to the database
